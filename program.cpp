#include <iostream>
#include <string>
#include "tree.h"
#include <fstream>
#include <sstream>
using namespace std;

void main()
{
	Tree<string> mytree;
	ifstream file("word.txt");
	string input;
	int lineCount = 49; //int 49 = char 1
	
	while (getline(file, input)) { //get the line in text file until no line that can be get
		stringstream tmp(input); //convert to stringstream to store each word
		string word = " ";

		while (getline(tmp, word, ' ')) //use blank space to get each word
		{
			word = word + " " + (char)lineCount;
			mytree.insert(word); //add each word to the tree
		}

		lineCount++; //go next line
	}

	mytree.inorder();
	system("pause");
}